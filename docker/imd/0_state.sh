#!/usr/bin/env bash

# despite the filename, this script sets up the state disk.

# the _really easy part_ - we're already mounted.
mountpoint -q /state && exit 0

# the easy part - the disk exists.
[[ -d /state ]] || mkdir /state
[[ -e /dev/disk/by-label/state ]] && { fsck -fy /dev/disk/by-label/state && mount /dev/disk/by-label/state /state ; exit $? ; }

# no luck? okay. let's go find a blank volume and format it.
# this plays with jq filtering to find non-removable disk volumes with no children.
diskq="$(lsblk --json | jq '[.blockdevices[] | select(.type=="disk") | select(.rm=="0" or .rm==false) | select(has("children")|not)]')"

# do we have _exactly one_? otherwise get outta here.
diskc="$(echo "${diskq}" | jq '. | length')" 
[[ "${diskc}" -eq "1" ]] || { echo "failed to get exactly one blank disk back" ; exit 1 ; }

# now get that device name
dname="$(echo "${diskq}" | jq -r '.[0].name')"

# check if we have a gpt or mbr partition table at this point
ptype="$(file -s "/dev/${dname}" | cut -d: -f2 | cut -d\; -f1)"

case "${ptype}" in
  "DOS/MBR boot sector") : ;; # already a partition, move on
  *) # make a partition table..
    parted "/dev/${dname}" mklabel gpt
  ;;
esac

# create a partition
parted "/dev/${dname}" mkpart '' ext2 1m 100%
udevadm settle

# create a file system
mkfs.ext4 "/dev/${dname}1" -Lstate

# wait for the device to show...
udevadm settle

# mount it
mount /dev/disk/by-label/state /state

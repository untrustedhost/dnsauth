#!/usr/bin/env bash

# broken out into separate piece for reusing state logic
mountpoint -q /state || exit 1

[[ -d /state/dnsauth ]] || mkdir /state/dnsauth

[[ -d /state/dnsauth/powerdns ]] || mkdir /state/dnsauth/powerdns

# mount over the regular powerdns dir
mountpoint -q /var/lib/powerdns || mount -o bind /state/dnsauth/powerdns /var/lib/powerdns

:

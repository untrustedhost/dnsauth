#!/usr/bin/env bash

unlock(){ : ; }

failboat() {
  echo "${@}" 1>&2
  unlock "${LOCK}"
  exit 1
}

my_service=dnsauth

# server ip address from here
V4XML="/run/untrustedhost/netxml/${my_service}.xml"
RUNTIME="/run/anycast-healthchecker/${my_service}.check-conf"
LOCK="/run/anycast-healthchecker/${my_service}.lock"
ADDXML="/run/untrustedhost/dnsauth.xml"

[[ -e "${V4XML}" ]] || failboat "no interface xml"

. /usr/lib/untrustedhost/shelllib/service-check.bash

update_rtconf() {
  local v zones keynames d t fqdn
  t=$(mktemp)
  {
    return_siaddr "${my_service}"
    fqdn="$(xmlstarlet sel -t -v dnsauth/@fqdn "${ADDXML}")"
    printf 'FQDN="%s"\n' "${fqdn}"
    printf '%s' 'ZONES=( '
    zones=($(xmlstarlet sel -t -v dnsauth/zone/@name "${ADDXML}"))
    for v in "${zones[@]}" ; do
      printf '"%s" ' "${v}"
    done
    printf ')\n'
    printf '%s' 'TSIGNAMES=( '
    keynames=($(xmlstarlet sel -t -v dnsauth/tsigkey/@name "${ADDXML}" | sed 's/\./_/g'))
    for v in "${keynames[@]}" ; do
      printf '"%s" ' "${v}"
    done
    printf ')\n'
    printf 'declare -A algo keydata\n'
    for v in "${keynames[@]}" ; do
      d=$(xmlstarlet sel -t -v 'dnsauth/tsigkey[@name="'"${v//_/.}"'"]/@algo' "${ADDXML}")
      printf '%s[%s]="%s"\n' "algo" "${v}" "${d}"
      d=$(xmlstarlet sel -t -v 'dnsauth/tsigkey[@name="'"${v//_/.}"'"]/@data' "${ADDXML}")
      printf '%s[%s]="%s"\n' "keydata" "${v}" "${d}"
    done
  } > "${t}"
  mv "${t}" "${RUNTIME}"
}

lock "${LOCK}"

[[ -e "${RUNTIME}" ]] || update_rtconf
[[ "${RUNTIME}" -nt "${V4XML}" ]] || update_rtconf
[[ "${RUNTIME}" -nt "${ADDXML}" ]] || update_rtconf

. "${RUNTIME}" || { rm "${RUNTIME}" ; failboat "failed to parse runtime config" ; }

[[ "${SIADDR}" ]] || { rm "${RUNTIME}" ; failboat "no server address in v4conf?!" ; }

# do I have a pdns_server process?
pgrep pdns_server > /dev/null || failboat "no pdns_server process"

# check for tsig keys
keyct="${#TSIGNAMES[@]}"
while read -r keyname keyalg rkeydata ; do
  skey="${keyname%?}"	# strip trailing .
  skey="${skey//./_}"	# turn other dots into _
  case " ${TSIGNAMES[*]} " in
    *" ${skey} "*)
      [ "${keyalg}" == "${algo[${skey}]}." ] && [ "${rkeydata}" == "${keydata[${skey}]}" ] && ((keyct--))
    ;;
  esac
done < <(pdnsutil --config-dir=/run/untrustedhost/powerdns list-tsig-keys)

[ "${keyct}" -eq 0 ] || failboat "unable to find all tsig keys"

# get a list of zones and just get their SOA record
for zone in "${ZONES[@]}" ; do
  read -r authns rest < <(dig +short -t SOA "${zone}." "@${SIADDR}")
  [[ "${authns}" != "${FQDN}." ]] && failboat "zone does not mach authdns server fqdn"
done

# look by this point everything worked, soooo :)
exit 0

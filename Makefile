packer/output-qemu/dnsauth.qcow2: packer/ephemeral/dnsauth.tar packer/ephemeral/installercore.iso
	-rm packer/output-qemu/dnsauth.qcow2
	cd packer && make output-qemu/dnsauth.qcow2

packer/output-qemu/dnsauth.qcow2.xz: packer/output-qemu/dnsauth.qcow2
	xz -T0 packer/output-qemu/dnsauth.qcow2

persistent.qcow2:
	qemu-img create -f qcow2 persistent.qcow2 1G

scratch-interactive: packer/output-qemu/dnsauth.qcow2 metadata/mddata.iso persistent.qcow2
	../../udstools/uds2fd.py 99:../ospf.home -- \
	qemu-system-x86_64 -nographic -m 192 -drive file=packer/output-qemu/dnsauth.qcow2,if=virtio \
                -drive file=persistent.qcow2,if=virtio \
		-cdrom metadata/mddata.iso \
		-device virtio-net-pci,netdev=n1,mac=52:54:00:4f:55:75 -netdev socket,id=n1,fd=99

packer/ephemeral/dnsauth.tar: build.sh docker/Dockerfile
	CODEBASE=dnsauth ./build.sh
	-docker rm export
	docker run --name export build/release true
	docker export export > packer/ephemeral/dnsauth.tar

packer/ephemeral/installercore.iso:
	docker run --rm=true -v $$(pwd)/packer/ephemeral:/workdir:Z registry.gitlab.com/untrustedhost/installenv
	sudo chown $$(whoami) packer/ephemeral/installercore.iso

metadata/mddata.iso: metadata/Makefile metadata/makeiso.sh metadata/mangle-example.sh metadata/virt-install.xml
	cd metadata && make mddata.iso
